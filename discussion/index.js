// [OBJECTIVE] Create a server-side app using Express Web Framework


// Relate this task to something that you do on a daily basis.


//[SECTION] append the entire app to our node package manager.

//package.json -> the "heart" if every node project.
// this also contains different meta data that describes the structure of the project


// scripts -> is used to declare and describe custom commands and keyword that can bu used
// to execute this project with the correct runtime environment

// NOTE:"start" is globally recognize amongst node projects and frameworks as the 'default'
// command script to execute a task/project
// however for *unconventional* keywords or comman you have to append the command "run"

// SYNTAX: npm run <custom commands>


//1. Identify and prepare the ingredients.
const express = require("express"); 

// express => will be used as the main component to create the server.
// we need to be able to gather/acquire the utilities and components needed that
// the express library will provide us.
// => require() -> directice used to get the library/components needed inside the module

// prepare the environment in which the project will be served

// [SECTION] Preaparing a Remote repository for our Node project

// NOTE: always DISABLE the node_modules folder
// WHY? 
// 1-> it will take up to much space in our repository making a lot more difficult
// to stage upon commitin the changes in our remote repo.
// 2.-> ie ever that you will deploy your node project on deployment platforms
// (heroku, netlify, vercel) the project will automotacally be rejected because node_modules
// is not recognized on various deployment platforms.

// HOW? using a .gitignore module



// [SECTION] Create a Runtime environment that automatically autofix
// all the changes in our app

// were going to use utility called nodemon.
// upon starting the entry point modulw with nodemone you will be able
// to the 'appned' the application with the proper run time environment
// allowing you to save time and effort upon commiting changes to your app
// console.log ('This will be our server');
// console.log ('NEW CHANGES');
// console.log('This is anohter change');
// you can even insert item like text art into your run time environment
console.log(`Welcome to our express API Server
████████████████████████████████████████
██████▀░░░░░░░░▀████████▀▀░░░░░░░▀██████
████▀░░░░░░░░░░░░▀████▀░░░░░░░░░░░░▀████
██▀░░░░░░░░░░░░░░░░▀▀░░░░░░░░░░░░░░░░▀██
██░░░░░░░░░░░░░░░░░░░▄▄░░░░░░░░░░░░░░░██
██░░░░░░░░░░░░░░░░░░█░█░░░░░░░░░░░░░░░██
██░░░░░░░░░░░░░░░░░▄▀░█░░░░░░░░░░░░░░░██
██░░░░░░░░░░████▄▄▄▀░░▀▀▀▀▄░░░░░░░░░░░██
██▄░░░░░░░░░████░░░░░░░░░░█░░░░░░░░░░▄██
████▄░░░░░░░████░░░░░░░░░░█░░░░░░░░▄████
██████▄░░░░░████▄▄▄░░░░░░░█░░░░░░▄██████
████████▄░░░▀▀▀▀░░░▀▀▀▀▀▀▀░░░░░▄████████
██████████▄░░░░░░░░░░░░░░░░░░▄██████████
████████████▄░░░░░░░░░░░░░░▄████████████
██████████████▄░░░░░░░░░░▄██████████████
████████████████▄░░░░░░▄████████████████
██████████████████▄▄▄▄██████████████████
████████████████████████████████████████
████████████████████████████████████████
`);    //use bacticks only